    
    <script src="<?php echo base_url() ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url() ?>assets/bs3/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url() ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.nicescroll.js"></script>

    <script src="<?php echo base_url() ?>assets/js/easypiechart/jquery.easypiechart.js"></script>
    <!--Sparkline Chart-->
    <script src="<?php echo base_url() ?>assets/js/sparkline/jquery.sparkline.js"></script>

    <?=$js_custom ?>

    <!--common script init for all pages-->
    <script src="<?php echo base_url() ?>assets/js/scripts.js"></script>