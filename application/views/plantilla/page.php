<?=$header ?>

<body class="<?=$body_class?>">
    <section id="container">
        <!--header start-->
        <?php echo $navbar ?>
        <!--header end-->
        <!--sidebar start-->
        <?=$sidebar ?>
        <!--sidebar end-->
        <!--main content start-->
        <?=$contenido ?>
        <!--main content end-->
    </section>
    <!-- Placed js at the end of the document so the pages load faster -->
    <!--Core js-->
    <?=$scripts ?>
    <!--script for this page-->
</body>

<!-- Mirrored from bucketadmin.themebucket.net/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Aug 2019 00:00:01 GMT -->

</html>