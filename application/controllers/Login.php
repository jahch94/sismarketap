<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{

		// if (!$this->session->userdata('logged_in')==true) {
		// 	redirect('login', 'refresh');
		// }

		$titulo = 'Login';

		$body_class = 'login-body';

		$css_custom = '';

		$js_custom = '';

		$array = array(
					'titulo' => $titulo,
					'body_class' => $body_class,
					'css_custom' => $css_custom,
					'js_custom' => $js_custom
					// 'usuario' => $this->session->userdata('usuario')
				);

		$vista = $this->load->view('login/login',$array, TRUE);

		$this->getTemplate($vista, $array);
	}

	public function getTemplate($view, $array){

		$data = array(
				'header' => $this->load->view('plantilla/header', $array, TRUE),
				'navbar' => '',
				'sidebar' => '',
				'contenido' => $view,
				'scripts' => $this->load->view('plantilla/scripts', $array, TRUE),
		);

		$this->load->view('plantilla/page', $data);
	}
}
